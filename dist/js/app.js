$(document).ready(function() {


    $(".slider1").slick({
        prevArrow: "<img class='a-left control-c prev slick-prev' src='dist/img/noibat/back-icon.png'>",
        nextArrow: "<img class='a-right control-c next slick-next' src='dist/img/noibat/next-icon.png'>",
        autoplay: false,
        autoplaySpeed: 2000,
    })

    $(".slider2").slick({
        prevArrow: "<img class='a-left control-c prev slick-prev' src='dist/img/noibat/back-icon.png'>",
        nextArrow: "<img class='a-right control-c next slick-next' src='dist/img/noibat/next-icon.png'>",
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 3
    })

    /*Equal height for column*/
    $(function() {
        $('.service-form').matchHeight({ property: 'min-height' });
    });

    /*Submenu trang gioi thieu*/
    var submenu = $('.submenu');
    //Hide submenu
    submenu
        .find('ul')
        .hide();
    submenu.each(function() {
        var $this = $(this);
        $this.click(function() {
            //Change image source
            var src = $this.find('span img').attr('src');
            var newsrc = (src == 'dist/img/gioithieu/plus.png') ? 'dist/img/gioithieu/minus.png' : 'dist/img/gioithieu/plus.png';
            $this.find('span img').attr('src', newsrc);
            //Make another submenu slideUp when click on an submenu
            $this.siblings('li')
                .find('ul')
                .slideUp(400);
            $this.find('ul').slideToggle(400);
        })
    })

    /*Hover menu*/
    $('li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).show();
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).hide();
    })

});
